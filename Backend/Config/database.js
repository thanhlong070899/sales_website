const mongoose = require('mongoose');

const main_project_db = mongoose.createConnection("mongodb://localhost:27017/main-project", { useNewUrlParser: true }, (error) => {
    if (!error) {
        console.log(`connect database success`);
    }
    else
    {
        console.log(`connect database failed`);
        console.log(error)
    }
}, 5000)

module.exports = main_project_db