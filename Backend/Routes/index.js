const express = require('express');
const router = express.Router();
const authRoute = require('./authRoute')
const productRoute = require('./productRoute')

const defaultRoutes = [
    {
        path: '/auth',
        route: authRoute,
    },
    {
        path: '/product',
        route: productRoute,
    }
];

defaultRoutes.forEach((route) => {
    router.use(route.path, route.route);
    console.log(route.path)
});

module.exports = router;