const express = require('express');
const router = express.Router();
const productController = require('../Controller/productController')
const {verifyToken, admin_permission} = require('../Service/Authentication')
const {upload} = require('../Middleware/uploadMiddleware')

router.post('/create', verifyToken, admin_permission, upload.single('image'),productController.createProduct)
router.get('/getproducts',productController.getProducts)

module.exports = router