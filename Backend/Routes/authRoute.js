const express = require('express');
const router = express.Router();
const authController = require('../Controller/authController')
const {verifyToken} = require('../Service/Authentication')

router.post('/login', authController.login)
router.get('/info',verifyToken,authController.getInfo)
router.get('/logout',verifyToken,authController.logOut)
router.post('/register', authController.register)
router.post('/listUser', authController.listUser)

module.exports = router