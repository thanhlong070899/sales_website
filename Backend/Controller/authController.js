const User = require('../Model/User.model')
const { loginval, registerval } = require('../Validaton/auth.validation')
const jwt = require('jsonwebtoken')
const { access_token } = require('../Service/Authentication')
const pick = require('../utils/pick')
const {userService} = require('../Service')

const login = async (req, res) => {
    try {
        const { email, password } = req.body
        const { error } = await loginval.validate(req.body)
        if (error)
            return res.status(200).send({ message: error.message, success: false })
        var user = await User.findOne({ email: email })
        if (!user)
            return res.status(200).send({ message: 'User is not found', success: false })
        if (!(await user.isPasswordMatch(password)))
            return res.status(200).send({ message: 'Password is not correct', success: false })
        const token = await access_token(req.body)
        user.token = token
        await user.save()
        return res.status(200).send({ message: 'Login success', access_token: token, success: true })
    } catch (error) {
        console.log(error)
        return res.status(500).send({ message: error.message, success: false })
    }
}

const getInfo = async (req, res) => {
    try {
        return res.send(req.user)
    } catch (error) {
        return res.send({ message: error.message })
    }
}
const logOut = async (req, res) => {
    try {
        req.user.token = null
        await req.user.save()
        return res.send({ message: 'Logut success' })
    } catch (error) {
        return res.send({ message: error.message })
    }
}
const register = async (req,res) =>{
    try {
        const { error } = await registerval.validate(req.body)
        if (error)
            return res.status(200).send({ message: error.message, success: false })
        if(await User.findOne({email:req.body.email}))
        return res.status(200).send({message: 'Email already exists'})
        userService.createUser(req.body)
        return res.status(200).send({message: 'success', success: true})
    } catch (error) {
        return res.status(500).send({message: error.message, success : false})
    }
}

const listUser = async (req, res) => {
    try {
        // const queryString = {}
        // if (req.query.keyword)
        //     queryString.$or = [
        //         { 'name': { $regex: req.query.keyword, $options: 'i' } },
        //         { 'email': { $regex: req.query.keyword, $options: 'i' } }
        //     ]
        // let totalPage = await User.countDocuments(queryString)
        // let page = (req.query.page) ? parseInt(req.query.page) : 1
        // let perPage = (req.query.perPage) ? parseInt(req.query.perPage) : 2
        // const users = await User.find(queryString).skip((page - 1) * perPage).limit(perPage)
        // const data = { totalPage: totalPage, page: page, perPage: perPage, data: users }
        const filter = pick(req.query, ['name', 'email']);
        const options = pick(req.query, ['sortBy', 'limit', 'page']);
        const result = await userService.queryUsers(filter, options);
        return res.status(200).send({ message: 'success', success: true, data: result })
    } catch (error) {
        return res.status(500).send({ message: error.message, success: false })
    }
}
module.exports = { login, logOut, getInfo, register, listUser }