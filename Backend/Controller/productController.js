const Product = require('../Model/Product.model')
const { getProductsVal, createProductVal } = require('../Validaton/product.validation')
const pick = require('../utils/pick')
const { productService } = require('../Service')

const createProduct = async (req, res) => {
    try {
        const { error } = await createProductVal.validate(req.body)
        if (error)
            return res.status(200).send({ message: error.message, success: false })
            console.log(req.file)
        if (req.file)
            req.body.image = req.file.destination+ req.file.originalname
        productService.createProduct(req.body)
        return res.status(200).send({ message: 'success', success: true })

    } catch (error) {
        return res.status(500).json({ success: false, message: error.message })
    }
}

const getProducts = async (req, res) => {
    try {
        const filter = pick(req.query, ['name', 'price']);
        const options = pick(req.query, ['sortBy', 'limit', 'page']);
        const result = await productService.queryProducts(filter, options);
        return res.status(200).send({ message: 'success', success: true, data: result })
    } catch (error) {
        console.log(error)
        return res.status(500).json({ success: false, message: error.message })
    }
}

module.exports = { createProduct, getProducts }