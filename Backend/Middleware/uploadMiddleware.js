const multer = require('multer');


const storage = multer.diskStorage({
    destination: (req,res,cb) =>{
        cb(null, 'Image/')
    },
    filename: (req,file, cb) =>{
    cb(null,  Date.now().toString() + file.originalname )
    }
})

const fileFilter = (req,file,cb) =>{
    console.log(file.mimetype)
    if(file.mimetype === 'image/jpg' || file.mimetype === 'image/png')
    cb(null,true)
    else
    cb(null,false)
}

const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 5
    },
    fileFilter: fileFilter
})




module.exports = {upload}