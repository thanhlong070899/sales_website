const Joi = require('joi');


const createProductVal = Joi.object({
    name: Joi.string().required(),
    price: Joi.string().required(),
    description : Joi.string()
});

const getProductsVal = Joi.object({
    Name: Joi.string(),
    Price: Joi.number(),
});

module.exports = { createProductVal, getProductsVal }