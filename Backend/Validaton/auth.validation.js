const Joi = require('joi');


const loginval = Joi.object({
    email: Joi.string().required(),
    password: Joi.string().required(),
});

const registerval = Joi.object({
    email: Joi.string().email().required(),
    name: Joi.string().required(),
    password: Joi.string().required(),
    father: Joi.array()
})
module.exports = { loginval, registerval }