const express = require('express')
require('dotenv').config({ path: './Config/.env' })
const app = express()
const cors = require('cors')
app.use(express.json())
app.use(cors())
require('body-parser')
const morgan = require('morgan')

app.use(morgan('combined'))
app.use('/image',express.static('Image'))
const route = require ('./Routes/index.js')

app.use('/v1', route)

app.listen(3001, () => { console.log('connected port 300') })