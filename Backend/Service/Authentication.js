const jwt = require('jsonwebtoken')
const User = require('../Model/User.model')

const verifyToken = async (req, res, next) => {
    try {
        const authorization = req.headers['authorization'];
        if (!authorization)
            return res.sendStatus(401)
        const token = authorization.split(' ')[1]
        if (!token)
            return res.sendStatus(401)
        jwt.verify(token, process.env.ACCESS_TOKEN, async (err, data) => {
            if (err) return res.sendStatus(403)
            req.user = await User.findOne({ email: data.email, token: token }).select('-password')
            if(!req.user)
            return res.send({message: 'Token too old'})
            next()
        })
    } catch (error) {
        return res.send({ 'message: ': error.message })
    }

}

const access_token = async (data) => {
    const accessToken = await jwt.sign(data, process.env.ACCESS_TOKEN, { expiresIn: '1h' })
    return accessToken
}

const admin_permission  = async (req,res,next) =>{
    try {
        if(req.user.role != 'admin')
            return res.status(200).json({success : false , message: 'You do not have permission'})
        next()
    } catch (error) {
        return res.send({ 'message: ': error.message })
    }
}


module.exports = { verifyToken, access_token, admin_permission }