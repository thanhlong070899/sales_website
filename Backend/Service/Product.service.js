const  Product  = require('../Model/Product.model');


const createProduct = async (productBody) => {
    return Product.create(productBody);
  };

  const queryProducts = async (filter, options) => {
    const products = await Product.paginate(filter, options);
    return products;
  };
  
  module.exports = {createProduct, queryProducts}