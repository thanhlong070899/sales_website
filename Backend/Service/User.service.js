const  User  = require('../Model/User.model');


const createUser = async (userBody) => {
    if (await User.isEmailTaken(userBody.email)) {
      throw new ApiError(httpStatus.BAD_REQUEST, 'Email already taken');
    }
    return User.create(userBody);
  };

  const queryUsers = async (filter, options) => {
    const users = await User.paginate(filter, options);
    return users;
  };
  
  module.exports = {createUser, queryUsers}