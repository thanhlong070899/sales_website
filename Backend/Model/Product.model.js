const mongoose = require('mongoose');
const main_project_db = require('../Config/database')
const { paginate } = require('../plugins/paginate')

const productSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      trim: true,
    },
    price: {
      type: Number,
      required: true,
      trim: true,
      lowercase: true,
    },
    description : {
        type: String,
        default: "",
        trim: true,
    },
    image:{
      type: String,
      default:''
    }
  },
  {
    timestamps: true,
  }
);

productSchema.plugin(paginate);

const Product = main_project_db.model('Product', productSchema);

module.exports = Product;
