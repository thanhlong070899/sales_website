const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const validator = require('validator')
const main_project_db = require('../Config/database')
const { paginate } = require('../plugins/paginate')

const userSchema = mongoose.Schema(
  {
    name: {
      type: String,
      required: true,
      trim: true,
    },
    email: {
      type: String,
      required: true,
      unique: true,
      trim: true,
      lowercase: true,
      validate(value) {
        if (!validator.isEmail(value)) {
          throw new Error('Invalid email');
        }
      },
    },
    role: {
      type: String,
      default: "user",
      lowercase: true,
      required: true,
      enum: ['user', 'admin']
    },
    password: {
      type: String,
      required: true,
      trim: true,
      minlength: 6,
      // validate(value) {
      //   if (!value.match(/\d/) || !value.match(/[a-zA-Z]/)) {
      //     throw new Error('Password must contain at least one letter and one number');
      //   }
      // },
      private: true, // used by the toJSON plugin
    },
    isEmailVerified: {
      type: Boolean,
      default: false,
    },
    token: {
      type: String,
      default: null,
      select: false
    },
    father: [{
      type: mongoose.Schema.ObjectId,
    }]
  },
  {
    timestamps: true,
  }
);

userSchema.plugin(paginate);

userSchema.statics.isEmailTaken = async function (email, excludeUserId) {
  const user = await this.findOne({ email, _id: { $ne: excludeUserId } });
  return !!user;
};

userSchema.methods.isPasswordMatch = async function (password) {
  const user = this;
  return bcrypt.compare(password, user.password);
};

userSchema.pre('save', async function (next) {
  const user = this;
  if (user.isModified('password')) {
    user.password = await bcrypt.hash(user.password, 8);
  }
  next();
});

const User = main_project_db.model('User', userSchema);

module.exports = User;
