
import './App.css';
import Create from "./Component/create.component"
import Index from "./Component/index.component"
import Login from "./Component/Auth/login.component"
import { BrowserRouter  as Router , Route, Routes} from 'react-router-dom';
function App() {
  return (
    <Router>
    <div className="container" >
      <Routes>
          <Route  path='/create' element={ <Create /> } />
          <Route  path='/Login' element={ <Login /> } />
          <Route  path='/index' element={ <Index /> } />
      </Routes>
    </div>
  </Router>
  );
}

export default App;
