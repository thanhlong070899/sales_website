import React, { Component } from 'react';

import axios from 'axios'

class  Create extends Component {

    constructor(props) {
        super(props);
        this.onChangeName = this.onChangeName.bind(this);
        this.onChangeEmail  = this.onChangeEmail.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);
        this.onSubmit = this.onSubmit.bind(this);

        this.state = {
            name: '',
            email: '',
            password:''
        }
    }
    onSubmit(e) {
        e.preventDefault();
        const obj = {
            name: this.state.name,
            email: this.state.email,
            password: this.state.password
        };
        axios.post('http://localhost:4000/auth/register', obj)
            .then(res => console.log(res.data));

        this.props.history.push('/index');
    }

    onChangeName(e) {
        this.setState({
            name: e.target.value
        });
    }
    onChangeCompany(e) {
        this.setState({
            company: e.target.value
        })
    }
    onChangeAge(e) {
        this.setState({
            password: e.target.value
        })
    }

    render() {
        return (
            <div style={{marginTop: 10}}>
            <h3>Register User</h3>
            <form  onSubmit={this.onSubmit}>
                <div className="form-group">
                    <label>Add Name :  </label>
                    <input type="text" className="form-control"/>
                </div>
                <div className="form-group">
                    <label>Add Email : </label>
                    <input type="text" className="form-control"/>
                </div>
                <div className="form-group">
                    <label>Add Password : </label>
                    <input type="text" className="form-control"/>
                </div>
                <div className="form-group">
                    <input type="submit" value="Register User" className="btn btn-primary"/>
                </div>
            </form>
        </div>
        )
    }
}

export default Create