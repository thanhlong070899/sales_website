import React, { useState } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import './login.css'
import axios from 'axios'
import { BrowserRouter  as useNavigate} from 'react-router-dom';


function Login() {
    const [password, setPassword] = useState("");
    const [email, setEmail] = useState("");
    const [loginMessage, setloginMessage] = useState('')
    const [loginStatus, setloginStatus] = useState(false)
    const navigate = useNavigate();
    const loginEvent = (event) => {
        axios.post('http://localhost:3001/v1/auth/login', { email, password })
            .then(function (response) {
                response.data.success ? setloginStatus(true) : setloginStatus(false)
                setloginMessage(response.data.message)
                if( response.data.success)
                {
                    navigate('/info');
                }
            })

    };
    var className = ''
    loginStatus ? className = 'text-success form-text' : className = 'text-danger form-text'
    const loginSubmit = (e) => {
        e.preventDefault();
        loginEvent();
    };
    return (
        <div className="App">
            <div className="container">
                <div className="row d-flex justify-content-center">
                    <div className="col-md-4">
                        <form id="loginform" onSubmit={loginSubmit}>
                            <div className="form-group">
                                <label>Email address</label>
                                <input
                                    type="email"
                                    className="form-control"
                                    id="EmailInput"
                                    name="EmailInput"
                                    aria-describedby="emailHelp"
                                    placeholder="Enter email"
                                    onChange={(event) => setEmail(event.target.value)}
                                />
                            </div>
                            <div className="form-group">
                                <label>Password</label>
                                <input
                                    type="password"
                                    className="form-control"
                                    id="exampleInputPassword1"
                                    placeholder="Password"
                                    onChange={(event) => setPassword(event.target.value)}
                                />
                            </div>
                            <div className="form-group form-check">
                                <input
                                    type="checkbox"
                                    className="form-check-input"
                                    id="exampleCheck1"
                                />
                                <label className="form-check-label">Check me out</label>
                            </div>
                            <button type="submit" className="btn btn-primary">
                                Login
                            </button>

                        </form>
                        <small id="loginsuccess" className={className}>
                            {loginMessage}
                        </small>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default Login;
