
import React, { Component } from 'react';

class TableRow extends Component {
  render() {
    return (
        <tr style={{marginLeft: 30, textAlign: 'center'}}>
            <td>
            {this.props.obj.id}
          </td>
          <td>
            {this.props.obj.name}
          </td>
          <td>
            {this.props.obj.role}
          </td>
          <td>
            {this.props.obj.email}
          </td>
          <td>
            {this.props.obj.isEmailVerified.toString()}
          </td>
          <td>
            <button className="btn btn-primary">Edit</button>
          </td>
          <td>
            <button className="btn btn-danger">Delete</button>
          </td>
        </tr>
    );
  }
}

export default TableRow;