import React, { Component } from 'react';
import axios from 'axios';
import TableRow from './table.component'

class  Index extends Component {

    constructor(props) {
        super(props);
        this.state = {user: []};
    }

    componentDidMount() {
        axios.get('http://localhost:3000/v1/users')
            .then(response => {
                this.setState({user: response.data.results});
            })
            .catch(function (error) {
                console.log(error);
            })
    }

    tableRow() {
        return this.state.user.map(function (object, i) {
            console.log("object ", object.isEmailVerified)
            return <TableRow obj={object} key={i}/>;
        });
    }

    render() {
        return (
            <div>
            <h3 align="center">Persons List</h3>
            <table className="table table-striped" >
                <thead>
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>role</th>
                    <th>Email</th>
                    <th>isEmailVerified</th>
                    <th colSpan="2">Action</th>
                </tr>
                </thead>
                <tbody>
                {this.tableRow()}
                </tbody>
            </table>
        </div>
        )
    }
}

export default Index